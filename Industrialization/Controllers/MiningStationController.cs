﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Industrialization.Controllers {
    
    public class MiningStationController : MonoBehaviour {

        private string material;
        private Dictionary<String, int> inv;
        private int value;
        Text text;
        Constructable construct;

        public GameObject parent;
        

        // Use this for initialization
        void Start()
        {
            material = "Titanium";
            text = parent.GetComponentInChildren<Text>();
            construct = parent.GetComponent<Constructable>();

            InitializeDictionary();
            if (construct == null)
            {
                Console.WriteLine("ERROR: Industrialization: MiningStationController: Parent constructable component not found");
            }

            InvokeRepeating("MiningUpdate", 1, 2);
        }

        void MiningUpdate()
        {
            Console.WriteLine("YOLO - mining Update");
            inv.TryGetValue("Titanium", out int val);
            val += 1;
            inv["Titanium"] = val;
            Console.WriteLine("Titanium: " + val);
            inv.TryGetValue("Diamond", out val);
            val += 1;
            inv["Diamond"] = val;
            if (construct.constructed)
            {
                UpdateMessage();
            }

        }

        public void UpdateMessage()
        {
            Console.WriteLine("YOLO - UpdateMessage");
            string displayString = "";
            foreach(KeyValuePair<string, int> entry in inv)
            {
                if(entry.Value > 0)
                {
                    Console.WriteLine("Loopdy loo");
                    displayString = displayString + entry.Key + " : " + entry.Value + "/n";
                }
            }
            Console.WriteLine("String: " + displayString);
            text.text = displayString;
            
        }

        private void InitializeDictionary()
        {
            inv.Add("Titanium", 0);
            inv.Add("Diamond", 0);
            inv.Add("Lithium", 0);
            inv.Add("Copper", 0);
            inv.Add("Silver", 0);
            inv.Add("Gold", 0);
        }

    }

}
