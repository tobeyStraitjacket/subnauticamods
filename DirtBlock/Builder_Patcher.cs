﻿using Harmony;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using UnityEngine;
using DirtBlock.Util;

namespace DirtBlock {


    [HarmonyPatch(typeof(Builder))]
    [HarmonyPatch("Update")]
    internal class Builder_Update_Patcher {
        [HarmonyPrefix]
        public static void Postfix()
        {

            Vector3 ghostModelPos;

            GameObject ghostmodel = Builder.ghostModel;

            foreach(string str in DirtBlockConfig.snappingBlocks)
            {
                bool foundOne = false;
                if(Builder.prefab.name == str)
                {
                    foundOne = true;
                }

                if (!foundOne)
                {
                    return;
                }
            }

            if (ghostmodel == null)
            {
                Console.WriteLine("ERROR:DirtBlock:Builder_Patcher - could not find reflection info for GhostModel");
            }
            else
            {
                ghostModelPos = ghostmodel.transform.position;
                Collider[] colliders = Physics.OverlapSphere(ghostModelPos, 1.5f);

                float closest = 1.0f;
                GameObject nearestBlock = null;

                foreach (Collider col in colliders)
                {
                    if (col.gameObject.name == "SnapBlock")
                    {
                        if (Vector3.Distance(col.gameObject.transform.position, ghostModelPos) < closest)
                        {
                            nearestBlock = col.gameObject;
                        }

                    }
                }
                if (nearestBlock != null)
                {
                    Builder.ghostModel.transform.SetParent(nearestBlock.transform, false);
                    Builder.ghostModel.transform.localPosition = -1f * DirtBlockUtils.CalculateOffset(nearestBlock.transform.position, ghostModelPos);
                    Builder.ghostModel.transform.rotation = nearestBlock.transform.rotation;


                    Builder.placePosition = ghostmodel.transform.position;
                    Builder.placeRotation = ghostmodel.transform.rotation;

                }

            }

        }
    }
  
}
